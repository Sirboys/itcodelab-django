from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView

from core.forms import BookCreateForm
from core.models import Book
from django.template import loader


# Create your views here.


class BooksListView(ListView):
    model = Book
    template_name = "books.html"
    paginate_by = 100  # if pagination is desired

    def get_queryset(self):
        filter_val = self.request.GET.get('filter', '')
        new_context = Book.objects.filter(
            title__contains=filter_val,
        )
        return new_context

    def get_context_data(self, **kwargs):
        context = super(BooksListView, self).get_context_data(**kwargs)
        context['filter'] = self.request.GET.get('filter', '')
        return context


class BooksDetailView(DetailView):
    model = Book
    template_name = "book.html"


class BooksAddView(CreateView):
    model = Book
    template_name = "book_add.html"

    def get(self, request, *args, **kwargs):
        context = {'form': BookCreateForm()}
        return render(request, 'book_add.html', context)

    def post(self, request, *args, **kwargs):
        form = BookCreateForm(request.POST)
        if form.is_valid():
            book = form.save()
            book.save()
            return HttpResponseRedirect(reverse_lazy('core:books'))
        return render(request, 'book_add.html', {'form': form})


class BooksUpdateView(UpdateView):
    model = Book
    template_name = "book_add.html"
    fields = ("title", "author", "genre", "pages")
    # form_class = BookCreateForm



def index(request):
    count = Book.objects.count()
    template = loader.get_template("index.html")
    return HttpResponse(template.render({"count": count}, request))
