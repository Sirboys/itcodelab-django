from django.test import TestCase

from core.factory import BookFactory
from core.models import Book


class AnimalTestCase(TestCase):
    def setUp(self):
        self.test_book = BookFactory.create()

    def test_Book(self):
        self.assertEqual(self.test_book.title, Book.objects.get(id=1).title)
