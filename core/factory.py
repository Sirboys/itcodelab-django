import factory
from faker import Faker

from core.models import Book

faker = Faker()


class BookFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Book

    title = factory.LazyAttribute(lambda _: faker.name())
    genre = factory.LazyAttribute(lambda _: faker.random.choice(("Фантазия", "Поэма", "Роман")))
    author = factory.LazyAttribute(lambda _: faker.name())
    pages = factory.LazyAttribute(lambda _: faker.random.randint(10, 1000))
