from django.db import models


# Create your models here.
class Library(models.Model):
    address = models.TextField(help_text="Адрес")
    # books = models.ManyToManyField(Book)


class Book(models.Model):
    title = models.CharField(max_length=255, help_text="Название")
    genre = models.CharField(max_length=255, help_text="Жанр")
    author = models.CharField(max_length=255, help_text="Автор")
    pages = models.IntegerField()
    # library = models.ForeignKey(to=Library, on_delete=models.PROTECT, null=True)


class Subscriber(models.Model):
    name = models.CharField(max_length=255)
    library = models.ManyToManyField(Library)


if __name__ == '__main__':
    Subscriber.objects.acount()
