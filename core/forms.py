from django.forms import forms

from core.models import Book
from django.forms import ModelForm


class BookCreateForm(ModelForm):
    class Meta:
        model = Book
        fields = ("title", "author", "genre", "pages")

# class BookUpdateForm(ModelForm):
